(function($) {
    "use strict";

    AWSCognito.config.region = 'us-west-2';

    var SessionApp = function() {
        // Private variables
        var poolData = {
            UserPoolId : 'us-west-2_KEb2HUsSn',
            ClientId : '7upflomf202qos474641o861co'
        };
        var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
        var self = this;
        var currentUser = null;
        var cookieTimer = null;

        self.init = function() {
            //self.verifyAuthentication();
        }

        self.verifyAuthentication = function(doNotReload, callback) {
            var cookie = getCookie('access');
            currentUser = userPool.getCurrentUser();
            if(!cookie)
            {
                if(currentUser != null) {
                    currentUser.getSession(function (err, session) {
                        if(err)
                            console.log(err);
                        else {
                            setUserSessionCookies(session, doNotReload);
                            if(callback)
                                callback();
                        }
                    });
                }
                else if(window.location.pathname !== '/')
                    window.location = '/';
            }
            if(callback)
                callback();
            //else
            //    console.log(cookie);
            
            cookieTimer = setInterval(function() { self.verifyAuthentication(true); }, 60 * 60 * 1000);
        }

        function setUserSessionCookies(session, doNotReload) {
            var expirationDate = new Date();
            expirationDate.setHours(expirationDate.getHours() + 1);
            document.cookie = 'access=' + session.getAccessToken().getJwtToken() + '; expires=' + expirationDate.toUTCString() + '; path=/;';
            if(!doNotReload)
                window.location.reload();
        }

        self.registerNewUser = function(email, password) {
            var attributeList = [];
            var username = email.replace('@', '');
            var dataEmail = {
                Name : 'email',
                Value : email
            };
            var attributeEmail = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataEmail);
            
            attributeList.push(attributeEmail);
            
            userPool.signUp(username, password, attributeList, null, function(err, result){
                if (err) {
                    bootbox.alert(err);
                    return;
                }
                confirmUser(username, password);
            });
        }

        function confirmUser(username, password) {
            bootbox.prompt("Please check your email and enter your verification code", function(code) {
                var userData = {
                    Username : username,
                    Pool : userPool
                };
                var cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
                cognitoUser.confirmRegistration(code, true, function(err, result) {
                    if (err) {
                        alert(err);
                        return;
                    }
                    return void self.authenticateUser(username, password);
                });
            });
        }

        self.authenticateUser = function(email, password) {
            var username = email.replace('@', '');
            var authenticationData = {
                Username : username,
                Password : password
            };
            var authenticationDetails = new AWSCognito.CognitoIdentityServiceProvider.AuthenticationDetails(authenticationData);
            var userData = {
                Username : authenticationData.Username,
                Pool : userPool
            };
        
            currentUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
            currentUser.authenticateUser(authenticationDetails, {
                onSuccess: function (result) {
                    self.toggleLoader(true);
                    setUserSessionCookies(result);
                },
        
                onFailure: function(err) {
                    if(err.code == "UserNotConfirmedException")
                        return void confirmUser(username, password);

                    bootbox.alert(err);
                },
                mfaRequired: function(codeDeliveryDetails) {
                    // Using a lambda for auto confirmation, too heavy duty for this particular app
                    bootbox.prompt('Please input verification code', function(verificationCode) {
                        var result = currentUser.sendMFACode(verificationCode, this);
                    });
                }
            });
        }

        self.resetPassword = function(email) {
            var userData = {
                Username : email,
                Pool : userPool
            };
        
            currentUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
            currentUser.forgotPassword({
                onSuccess: function (result) {
                    console.log('call result: ' + result);
                },
                onFailure: function(err) {
                    alert(err);
                },
                inputVerificationCode() {
                    bootbox.prompt('Please input verification code ', function(code) {
                        bootbox.prompt('Enter new password ', function(newPassword) {
                            currentUser.confirmPassword(code, newPassword, this);
                            self.authenticateUser(email, newPassword);
                        });
                    });
                }
            });
        }

        self.setAuthHeader = function(xhr) {
            // TODO: Attach error handler for detecting 401s?
            xhr.setRequestHeader('Authorization', getCookie('access'));
        }

        self.logout = function() {
            if (currentUser != null) {
               currentUser.signOut();
            }
            window.location = '/logout';
        }

        self.toggleLoader = function(show, callback) {
            var shower = show ? $('#loader') : $('#contents');
            var hider = show ? $('#contents') : $('#loader');
            hider.fadeOut('slow', function() {
                shower.fadeIn('slow', function() {
                    if(callback)
                        callback();
                });
            });
        }

        function getCookie(name) {
            var regexp = new RegExp("(?:^" + name + "|;\s*"+ name + ")=(.*?)(?:;|$)", "g");
            var result = regexp.exec(document.cookie);
            return (result === null) ? null : result[1];
        }
    }

    window.sessionApp = new SessionApp();
    sessionApp.init();
})(jQuery);