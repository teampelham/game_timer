(function() {
    "use strict";

    // Make a whole separate JS file for AWS Cognito stuffs
    // On initial load, it nabs the session. If no session, hard redirect to '/', otherwise set a session cookie
    // Expose a function that takes an XHR, attaches the session cookie to the header

    var LoginApp = function() {
        // Private variables
        var self = this;

        // cached DOM elements
        self.$registerLink = $('#registerLink');
        self.$backLink = $('#backLink');
        self.$signupBtn = $('#signupBtn');
        self.$emailRegInput = $('#emailRegInput');
        self.$passwordRegInput = $('#passwordRegInput');
        self.$passwordConfirmInput = $('#passwordConfirmInput');
        self.$usernameInput = $('#usernameInput');
        self.$passwordInput = $('#passwordInput');
        self.$forgotPwdLink = $('#forgotPwdLink');
        self.$loginBtn = $('#loginBtn');

        self.init = function() {
            sessionApp.verifyAuthentication(false, function() {
                sessionApp.toggleLoader(false);
                self.$registerLink.click(swapVisiblePanels);
                self.$backLink.click(swapVisiblePanels);
                self.$signupBtn.click(validateNewUser);
                self.$loginBtn.click(validateExistingUser);
                self.$forgotPwdLink.click(resetUserPassword);
                self.$emailRegInput.blur(validateUsername);
                self.$usernameInput.blur(validateUsername);
                self.$passwordInput.blur(validatePassword);
                self.$passwordRegInput.blur(validatePassword);
                self.$passwordConfirmInput.blur(validatePasswordsMatch);
            });
        }

        function resetUserPassword(e) {
            e.preventDefault();
            var username = self.$usernameInput.val();
            if(!username.length)
                return void bootbox.alert('Please enter your username');
            sessionApp.resetPassword(username);
        }

        function swapVisiblePanels(e) {
            e.preventDefault();
            var $link = $(e.currentTarget);
            var $target = $($link.attr('data-target'));
            var $current = $link.closest('section');

            $current.fadeOut('slow', function() {   // TODO: Do this with classes and css transitions
                $target.fadeIn('slow');
            });
        }

        function validateNewUser(e) {
            e.preventDefault();
            var valid = validateUser(self.$emailRegInput, self.$passwordRegInput, self.$passwordConfirmInput);

            if(valid)
                sessionApp.registerNewUser(valid.username, valid.password);
        }

        function validateExistingUser(e) {
            e.preventDefault();
            var valid = validateUser(self.$usernameInput,self.$passwordInput);

            if(valid)
                sessionApp.authenticateUser(valid.username, valid.password);
        }

        function validateUsername(e) {
            validateUser($(e.currentTarget));
        }

        function validatePassword(e) {
            validateUser(null, $(e.currentTarget));
        }

        function validatePasswordsMatch(e) {
            validateUser(null, self.$passwordRegInput, self.$passwordConfirmInput);
        }

        function validateUser(usernameInput, passwordInput, pwConfirmInput) {
            var valid = true;
            var rv = {};
            if(usernameInput) {
                var username = usernameInput.val();
                if(username.length < 3 || username.indexOf('@') == -1) {
                    valid = false;
                    usernameInput.prop('title', 'Not a valid email').parent().addClass('invalid');
                }
                else {
                    rv.username = username;
                    usernameInput.prop('title', '').parent().removeClass('invalid');
                }
            }

            if(passwordInput) {
                var password = passwordInput.val();
                if(!password.length) {
                    valid = false;
                    passwordInput.prop('title', 'Please enter your password').parent().addClass('invalid');
                    // TODO: validate password better (aws will do it for now)
                }
                else {
                    rv.password = password;
                    passwordInput.prop('title', '').parent().removeClass('invalid');
                }
            }
            
            if (pwConfirmInput) {
                var pwConfirm = pwConfirmInput.val();
                if(pwConfirm != rv.password) {
                    valid = false;
                    pwConfirmInput.prop('title', 'Passwords do not match').parent().addClass('invalid');
                }
                else {
                    pwConfirmInput.prop('title', '').parent().removeClass('invalid');
                }
            }
            if(valid)
                return rv;
            
            return false;
        }
    }
    
    window.loginApp = new LoginApp();
    loginApp.init();
})();
