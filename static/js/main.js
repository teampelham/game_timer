(function($) {
    "use strict";
    var base_url = 'https://s08vuuaosb.execute-api.us-west-2.amazonaws.com/prod';

    // Starting/stopping the timer
    // Start timer by setting a TimerStarted status on Subject with a timestamp
    // Begin a countdown timer (find a js plugin) 
        // On Timer expired
            // Flash the name in neon
            // Play an alarm
            // Make a lambda function that -
                // Set the TimerStarted status to expired (make a sub-document with aggregated time, status, and timestamp?)
                // Set the Subject Alterations to zero out the time
                // Send an SNS event
        // On Timer stopped
            // Updates Subject Alterations to subtract total time
            // Update Subject's status

    // TODO: Abstract the ajax calls
    var TimerApp = function() {
        var self = this;
        var activeTimers = [];
        var obnoxiousAlarm = new Audio('assets/Annoying_Alarm_Clock.mp3');

        // self.d DOM elements
        self.nameInput = $('#nameInput');
        self.timeInput = $('#timeInput');
        self.addNewModal = $('#addNewModal');
        self.adjustModal = $('#addRemoveTimeModal');
        self.timerGrid = $('.timer-grid');
        self.rowTemplate = $('.timer-grid>div:first');
        self.saveNewButton = $('#saveNewBtn');
        self.saveAdjustmentBtn = $('#saveAdjustmentBtn');
        self.logoutLink = $('#logoutLink');

        self.init = function() {
            sessionApp.verifyAuthentication(true, finishInit);
        }

        function finishInit() {
            $('[data-toggle="tooltip"]').tooltip({
                delay: { "show": 1000, "hide": 500 }
            });
            self.logoutLink.click(onClickLogout);
            self.saveNewButton.click(onClickSaveNew);
            self.saveAdjustmentBtn.click(onClickSaveAdjustment);
            self.timerGrid.on('click', 'a.delete-subject', onClickDelete)
                .on('click', 'a.adjust-time', onClickAdjust)
                .on('click', 'a.edit-time', onClickEdit)
                .on('click', 'a.toggle-timer', onClickToggleTimer);
            window.onbeforeunload = onBeforeUnloaded;
            fetchData();
        }

        /********** EVENT HANDLERS */
        function onBeforeUnloaded(e) {
            if(self.timerGrid.find('.neon').length) {
                e.returnValue = "Timers will pause if you leave";
                addError(e.returnValue);
                return e.returnValue;
            }
        }

        function onClickSaveAdjustment(e) {
            e.preventDefault();
            var adjustment = parseInt(self.adjustModal.find('#adjustmentInput').val());
            var comment = self.adjustModal.find('#commentInput').val();
            var name = self.adjustModal.data('name');
            var payload = { name: name, adjustmentTime: adjustment, comment: comment };

            if(adjustment === 0)
                return void addError('Adjustment time must be non-zero');

            doSomeAjax('/subject-update', payload, 'POST', function(rowData) {
                if(rowData.Attributes)
                    rowData = rowData.Attributes;
                var row = self.timerGrid.find('.flex-grid[data-name="' + name + '"]');
                handleTimeLeft(rowData, row);
                self.adjustModal.modal('hide');
                self.adjustModal.find('#adjustmentInput').val('');
                self.adjustModal.find('#commentInput').val('');
            });
        }

        function onClickAdjust(e) {
            e.preventDefault();
            var row = $(this).closest('.flex-grid');
            var name = row.attr('data-name');
            self.adjustModal.data('name', name).find('h4.modal-title').html("Add/Subtract " + name + "'s daily time");
            self.adjustModal.modal('show');
        }

        function onClickLogout(e) {
            e.preventDefault();
            sessionApp.toggleLoader(true, function() {
                sessionApp.logout();
            });
        }

        function onClickToggleTimer(e) {
            e.preventDefault();
            var row = $(this).closest('.flex-grid');
            var status = row.attr('data-status') || '0';          
            var name = row.attr('data-name'); 
            status = status === '0' ? '1' : '0';
            bootbox.confirm((status == '0' ? 'Stop ' : 'Start ') + name + "'s timer?", function(result) { result && toggleTimer(name, status); });
        }

        function onClickDelete(e) {
            e.preventDefault();
            var row = $(this).closest('.flex-grid');
            var name = row.attr('data-name');
            bootbox.confirm({
                title: 'Are you sure?',
                message: 'Really delete "' + name + '"? This can never be undone. Ever.', 
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> Oh... Wait'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Just Do It'
                    }
                },
                callback: function(result) {
                    if(result)
                        deleteSubject(name, row);
                }
            });
        }

        function onClickEdit(e) {
            e.preventDefault();
            var row = $(this).closest('.flex-grid');
            var name = row.attr('data-name');
            bootbox.prompt("Enter a new default time", function(result) { 
                if(result)
                    updateSubjectTime(name, result, row); 
            });
        }

        function onClickSaveNew(e) {
            e.preventDefault();
            var name = self.nameInput.val();
            var time = self.timeInput.val();
            if(!validateAddData(name, time))
                return;

            var row = { name: name, defaultTime: time };
            doSomeAjax('/timedsubjects', row, 'POST', function() {
                self.addNewModal.modal('hide');
                self.nameInput.val('');
                self.timeInput.val('');
                renderRow(row, self.timerGrid.children().length - 1);
            });
        }

        /********** DELETE SUBJECT */
        function deleteSubject(name, row) {
            // TODO: Create a cron job lambda that runs once a week and deletes history
            doSomeAjax('/timedsubjects', { name: name }, 'DELETE', function() {
                row.slideUp();
                addSuccess(name + ' has been completely erased from existence.');
            });
        }

        /********** UPDATE SUBJECT DEFAULT TIME */
        function updateSubjectTime(name, newTime, row) {
            doSomeAjax('/timedsubjects', { name: name }, 'PATCH', function() {
                // TODO: Use 'total' from return value and make sure it's been reaggregated by the lambda
                row.slideUp(function(result) {
                    row.children('div:last').children('span:first').html(newTime);
                    row.slideDown();
                });
                
                addSuccess(name + ' shall henceforth be given ' + newName);
            });
        }

        /********** PAGE LOAD FETCH N RENDER */
        function fetchData() {
            doSomeAjax('/timedsubjects', null, 'GET', renderData);
        }

        function renderData(data) {
            sessionApp.toggleLoader(false, function() {
                for(var i = 0; i < data.Count; i++) {
                    renderRow(data.Items[i], i);
                }
            });
        }

        function renderRow(rowData, i) {
            // TODO: Handle this with templating
            var row = self.rowTemplate.clone().attr('data-name', rowData.name);
            row.find('.col-1-3 span:first').html(rowData.name);
            row.find('.delete-subject').prop('title', 'Delete ' + rowData.name);
            
            row.find('.edit-time').prop('title', 'Edit ' + rowData.name);
            row.find('.toggle-timer').prop("Start " + rowData.name + "'s timer");
            if(i % 2 === 1)
                row.addClass('alternate');
            handleTimeLeft(rowData, row);
            handleTimerStarted(rowData, row);
            row.removeClass('hidden').appendTo(self.timerGrid);
        }

        /********** START AND STOP TIMERS */
        function toggleTimer(name, status) {
            doSomeAjax('/toggle-timer', { name: name, status: parseInt(status) }, 'POST', handleTimerUI);
        }

        function handleTimerStopped(rowData, row) {
            if(typeof rowData['timer-status'] !== 'undefined' || rowData['timer-status'].status === '0') {
                row.removeClass('neon').attr('data-status', '0');
                clearInterval(activeTimers[rowData.name]);
                addSuccess(row.attr('data-name') + "'s timer has been stopped");
            }
        }

        function handleTimerStarted(rowData, row) {
            if(typeof rowData['timer-status'] !== 'undefined' && rowData['timer-status'].status == '1') {
                row.addClass('neon').attr('data-status', '1');
                activeTimers[rowData.name] = setInterval(updateTimeLeftInterval, 60 * 1000, rowData);
                addSuccess(row.attr('data-name') + "'s timer is counting down!");
            }
        }

        function updateTimeLeftInterval(rowData) {
            console.log('One minute less for ', rowData);
            var row = self.timerGrid.find('.flex-grid[data-name="' + rowData.name + '"]');
            if(typeof rowData['timer-status'] === 'undefined')
                rowData['timer-status'] = {};

            rowData['timer-status'].total = parseInt(row.attr('data-time')) - 1;
            doSomeAjax('/toggle-timer', { name: rowData.name, timerStart: rowData['timer-status'].timestamp }, 'PATCH', function() {
                handleTimeLeft(rowData, row);
            });
        }

        function handleTimerUI(result) {
            if(result.Attributes)
                result = result.Attributes;
            else if(!(result['timer-status'] && result['timer-status'].status)) {
                addError('Unable to start timer!');
                return void console.log(result);
            }
            console.log(result);
            var row = self.timerGrid.find('.flex-grid[data-name="' + result.name + '"]');
            if(result['timer-status'].status == '0')
                handleTimerStopped(result, row);
            else 
                handleTimerStarted(result, row);
                
        }

        /********** HELPER FUNCTIONS */
        function doSomeAjax(url, data, method, success, error) {
            sessionApp.verifyAuthentication(true, function() {
                $.ajax({
                    url: base_url + url,
                    data: JSON.stringify(data),
                    method: method,
                    contentType: 'application/json',
                    beforeSend: function(xhr){
                        sessionApp.setAuthHeader(xhr);
                    },
                    success: success,
                    error: error || handleError
                });
            });
        }

        function validateAddData(name, time) {
            var valid = true;
            if(!name || name.length < 2) {
                valid = false;
                // TODO: validation message
            }

            if(!time || isNaN(time)) {
                valid = false;
                // TODO: validation message
            }

            return valid;
        }

        function addError(message) {
            addAlert(message, 'error');
        }

        function addSuccess(message) {
            addAlert(message, 'success');
        }

        function addAlert(message, type) {
            // TODO: Use templating for this
            var autoDismiss = false;
            var alert = $('<div class="alert alert-dismissable alert-fixed fade in" role="alert"></div>');
            alert.append('<button class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>');

            switch(type) {
                case 'error':
                    alert.addClass('alert-danger');
                    alert.append('<strong>Uh Oh!</strong>&#160;');
                    break;
                case 'success': 
                    alert.addClass('alert-success');
                    alert.append('<strong>Success!</strong>&#160;');
                    autoDismiss = true;
                    break;
            }
            alert.append(message);
            alert.appendTo('body');
            if(autoDismiss)
                alert.fadeTo(5000, 500).slideUp(500, function() {
                    alert.slideUp(500);
                });
        }

        function handleTimeLeft(rowData, row) {
            var timeLeft = rowData.defaultTime;
            if(typeof rowData['timer-status'] !== 'undefined' && typeof rowData['timer-status'].total !== 'undefined')
                timeLeft = rowData['timer-status'].total;

            if(timeLeft == 0) {
                timerExpired(row);
            }
            else
                row.attr('data-time', timeLeft).find('.col-2-3 span:first').html(formatTime(timeLeft));
        }

        function handleError(res) {
            addError('Sum-Ting-WONG');
            console.log(res);
        }

        function formatTime(time) {
            var hours = parseInt(time / 60);
            var minutes = time % 60;
            return formatHours(hours) + formatMinutes(minutes);
        }

        function formatHours(hours) {
            if(!hours)
                return '';
            if(hours === 1)
                return '1hr ';
            return hours + 'hrs ';
        }

        function formatMinutes(minutes) {
            if(!minutes)
                return '';
            if(minutes === 1) 
                return '1min';
            return minutes + 'mins';
        }

        function timerExpired(row) {
            obnoxiousAlarm.currentTime = 0;
            obnoxiousAlarm.play();

            row.addClass('expired').attr('data-time', 0).find('.col-2-3 span:first').html('Expired!');
            bootbox.alert(row.attr('data-name') + "'s timer has expired!", function() {
                obnoxiousAlarm.pause();
            });
        }
    }

    window.timerApp = new TimerApp();
    timerApp.init();
})(jQuery);