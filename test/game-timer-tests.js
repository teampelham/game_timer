const expect = require('chai').expect;
const gameTimer = require('../source/game-timer');
const testUserId = '666';

describe('Game Timer', () => {
    describe.skip('Add New Subject', () => {
        it('is not undefined', () => {
            expect(typeof gameTimer.addNewSubject).to.not.equal('undefined');
        });
        it('adds the subject to DynamoDB', (done) => {
            gameTimer.addNewSubject(testUserId, 'Test Subject', 500, (addUserResult) => {
                expect(addUserResult).to.be.ok;
                done();
            }, error => { throw new Error(error); });
        });
    });
    describe('Edit Subject Default Time', () => {
        it('modifies default time in DynamoDB', (done) => {
            gameTimer.updateDefaultTime(testUserId, 'Test Subject', 200, (updateSubjectResult) => {
                expect(updateSubjectResult).to.be.ok;
                done();
            }, error => { throw new Error(error); });
        });
    });
    describe.skip('Add/Remove Time', () => {
        it('adds time to the daily total', () => {
            let timeAdded = gameTimer.addDailyTime(testUserId, 'Test Subject', 300);
            expect(timeAdded).to.equal(500);
        });
        it('removes time to daily total', () => {
            let timeSubtracted = gameTimer.addDailyTime(testUserId, 'Test Subject', 400);
            expect(timeSubtracted).to.equal(100);
        });
        it('aggregates all the times into the total', () => {
            let timeLeft = gameTimer.getTime(testUserId, 'Test Subject');
            expect(timeSubtracted).to.equal(100);
        });
    });
    describe.skip('Start/Stop Timer', () => {
        it('starts a timer', () => {
            let started = gameTimer.startTimer(testUserId, 'Test Subject');
            expect(started).to.equal(true);
        });
        it('get status of a timer', () => {
            let isStarted = gameTimer.isTimerOn(testUserId, 'Test Subject');
            expect(isStarted).to.equal(true);
        });
        it('stops a timer', () => {
            let stopped = gameTimer.stopTimer(testUserId, 'Test Subject');
            expect(stopped).to.equal(true);
        });
    });
    describe.skip('Delete Subject', () => {
        it('removes the subject from DynamoDB', () => {
            let gone = gameTimer.deleteSubject(testUserId, 'Test Subject');
            expect(gone).to.equal(true);
        });
    });
});