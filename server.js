var express = require('express')
    , logger = require('morgan')
    , bodyParser = require('body-parser')
    , cookie = require('cookie')
    , gameTimer = require('./source/game-timer')
    , aws = require('aws-sdk')
    , jwkToPem = require('jwk-to-pem')
    , jwt = require('jsonwebtoken')
    , template = require('pug');


var _gameTimerClientId = '1aqhs7hr5q6he3n1lgek6rh26v';
var AMAZON_CLIENT_ID = "AKIAJ63QWZMURQPNI5HA";
var _userPoolID = 'us-west-2_KEb2HUsSn';
var _gameTimerTable = 'game-timer-subject';

aws.config.update({
    region: "us-west-2"
});

var _docClient = new aws.DynamoDB.DocumentClient();

var app = express();

app.use(logger('dev'));
app.use(express.static(__dirname + '/static'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


// GET Home Page
app.get('/', function(req, res, next) {
    try {
        var jwt = getAccessToken(req);
        if(jwt !== null)
            return res.redirect('/home');

        var html = template.compileFile(__dirname + '/source/templates/login.pug');
        res.send(html({ title: 'Please log in' }));
    } catch(e) {
        next(e);
    }
});

app.get('/home', function(req, res, next) {
    try {
        var jwt = getAccessToken(req);
        if(jwt === null)
            return res.redirect('/');   // TODO: Refresh

        res.setHeader('Cache-Control', 'no-cache');
        var html = template.compileFile(__dirname + '/source/templates/homepage.pug');
        gameTimer.getSubjects(jwt.payload.sub, 
            data => res.send(html({ title: data.Count + " Kiddos on deck", username: "Dennis", giggity: data.Items, loggedIn: true })),
            err => next(err));
    } catch(e) {
        next(e);
    }
});

// GET Logout

app.get('/logout', function(req, res, next) {
    res.clearCookie('access');
    res.clearCookie('refresh');

    return res.redirect('/');
});

// 404 errors
app.use(function(req, res, next) {
    var err = new Error('not found');
    err.status = 404;
    next(err);
});

app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    var htmlFn = template.compileFile(__dirname + '/source/templates/error.pug');
    var html = htmlFn({
        title: 'Utter failure',
        message: err.message,
        error: err
    });
    res.send(html);
});

var jws = {"keys":[{"alg":"RS256","e":"AQAB","kid":"DKjTI8Y/AQvOZ5f0C8FaFCWs2wlEt0aVZKIm2Erg/k0=","kty":"RSA","n":"l6h5EsCYI0rmRC60lCfGTMumjd5WpVBxIhLZneYtwYxilsWzQcDax1FqxAyilcoY0M8EKM5quxr7v2KNiGPJ6-llYWWhdbmLOGhHYmV8P15IBDoXzI_D0A7U-g9Z1YvZTKaWwnDBnN996g1D7FBPcNL0CF54fl5wOM4pZfhvr7J86OlylX_f2w3Zh3efAtqlNcfYtN9AqYIBdKbtCShQMP8Xmh-fAhUqTbisu17k_MOwXdYUm9p-PdUZ9lHIzc4IhB-uANJnQvmlz3toI0ytsg_QN31PvlcCIUbQvy9tIqCDsGuT0t_yg2zDDhT1XT-B9I-XlumTefJLXuklhGxq6Q","use":"sig"},{"alg":"RS256","e":"AQAB","kid":"V7twS6wSfmtH/dZe6LgKc+sZPkNhvUTihF+BczDalME=","kty":"RSA","n":"j2G_e8YKcpgkP5A-mNOcjfWUq_FVp3DllV_7_K_LLVxWlkcVriTM0-RH7LhLdQz2S7H3nfLpYpfOwsSMFrdFfVqKxU0AwWFB-OuEf3PdkVCgep_R6THMKneKJ8x2WAV629R-_t4bGJZdss3YnWx1kX3NIQcvdz6WLlHE47-zWV-0q5p2FfBd7PQX2wAm4g-LgCNGoW3nzm4rlCA8qxRUD4EmK3lQ28FJQe7WQFlHUEv8bzjypQQ89DGwoqbwmCvZ-K6FHDzex-Twdvq2XXe7OgpvYBu8kHT_0AoHaT6vcTUG_wYmIwkm09mCLGy1i3StU66KY--4WlyPWk86mUwj2w","use":"sig"}]};

var createPems = function() {
    var pems = {};
    var keys = jws.keys;
    for(var i = 0; i < keys.length; i++) {
        //Convert each key to PEM
        var key_id = keys[i].kid;
        var modulus = keys[i].n;
        var exponent = keys[i].e;
        var key_type = keys[i].kty;
        var jwk = { kty: key_type, n: modulus, e: exponent};
        var pem = jwkToPem(jwk);
        pems[key_id] = pem;
    }
    return pems;
}

var _pems = createPems();

function getAccessToken(req) {
    var cookie = getAccessCookie(req);
    if(cookie === null)
        return null;
    
    var decodedJwt = jwt.decode(cookie, {complete: true});
    if(!isValidJwt(decodedJwt))
        return null;
    
    return decodedJwt;
}

function getAccessCookie(req) {
    var cookies = cookie.parse(req.headers.cookie || '');
    if(typeof cookies.access === 'undefined')
        return null;
    return cookies.access;
}

function isValidJwt(decodedJwt) {
    if(!decodedJwt)
        return false;

    if(decodedJwt.payload.iss !== 'https:\/\/cognito-idp.us-west-2.amazonaws.com\/us-west-2_KEb2HUsSn')
        return false;
    
    if(decodedJwt.payload.token_use !== 'access' && payload.token_use !== 'id')
        return false;
    
    var kid = decodedJwt.header.kid;
    var pem = _pems[kid];
    if (!pem) 
        return false;
    
    var currentTime = new Date().getTime();
    if(currentTime < decodedJwt.header.exp * 1000)
        return false;

    return true;
}

app.listen(process.env.PORT || 80, function() {
    console.log('Listening on http://localhost:' + (process.env.PORT || 80))
});