# README #

This is a simple UI built in Node.js for the sheer heck of it. My kids spend way too much time watching Netflix and playing video games, and I like to geek out on learning new things, so Game Timers was born.

### What is this? ###

* This is a quick and dirty app to set up timers for my kids to track how much time they're allowed to spend gaming or watching TV
    * Each kid gets a default amount of time per day
    * Time can be added or removed for things like reading books, doing chores, or smacking around younger brothers
    * Timers can be tracked individually and an obnoxious alarm goes off when they expire
* Game Timers is built with Node, Express, Pug, Sass, Grunt, and some other fun tools.
* It's currently leveraging several AWSs including
    * hosted on EC2 Linux AMI
    * authenticating with Cognito User Pools
    * calling an API Gateway
        * which leverages Lambdas
        * to interact with a DynamoDB data store

### What's next? ###

* On the development roadmap for Game Timers web
    * Validation (yes, it's not there yet)
    * Moving toward a more SPA-style architecture to avoid the background flicker on login/logout
    * Use of client-side pug templates
    * Better color schemes and removal of Bootstrap theme
    * Leveraging AWS SNS and cron-style Lambdas to continue timer countdowns after navigating away (currently they just pause)
* Integrations
    * Alexa is next up, voice interactions to add/remove time, start/stop timers, and check how much time remains for each 'subject'
    * Same for Google Home
    * iOS and Android mobile apps (native all the way!)