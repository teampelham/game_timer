module.exports = function(grunt) {
  grunt.initConfig({
    sass: {
      dist: {
        files: {
          'static/css/style.css' : 'source/sass/login.scss'
        }
      }
    },
    watch: {
        source: {
            files: ['source/sass/**/*.scss'],
            tasks: ['sass'],
            options: {
                livereload: true
            }
        }
    }
  });
  
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-sass');
  grunt.registerTask('default', ['sass']);
};