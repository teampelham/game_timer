var AWS = require('aws-sdk');

var GAME_TIMER_TABLE = 'game-timer-subject';

AWS.config.update({
    region: "us-west-2"
});

var _docClient = new AWS.DynamoDB.DocumentClient();

var createSubjectInsert = function (userID, name, time) {
    if(!name || name.length < 2) {
        console.error('Received bad name to insert');
        return null;
    }

    return { 
        TableName: GAME_TIMER_TABLE,
        Item: {
            'userid': userID,
            'name': name,
            'defaultTime': Number(time)
        } 
    };
}

var createSubjectUpdate = (userID, name, newTime) => {
    return {
        TableName: GAME_TIMER_TABLE,
        Key: {
            "userid": userID,
            "name": name
        },
        UpdateExpression: "set defaultTime = :t",
        ExpressionAttributeValues: {
            ":t": new Number(newTime)
        }
    };
}

module.exports = {
    
    addNewSubject: function (loggedInUserID, subjectName, defaultTime, success, fail) {
        var subjectInsertParams = createSubjectInsert(loggedInUserID, subjectName, defaultTime);
        _docClient.put(subjectInsertParams, function(err, data) {
            if(err) 
                fail(err);
            else 
                success(data);
        });
    },

    updateDefaultTime: function(loggedInUserID, subjectName, defaultTime, success, fail) {
        var subjectUpdateParams = createSubjectUpdate(loggedInUserID, subjectName, defaultTime);    
        _docClient.update(subjectUpdateParams, function(err, data) {
            if(err) 
                fail(err);
            else 
                success(data);
        });
    },

    getSubjects: function(loggedInUserID, success, fail) {
        var params = {
            TableName: GAME_TIMER_TABLE,
            Key: {
                "userid": loggedInUserID
            }
        };

        _docClient.scan(params, function(err, data) {
            if(err) 
                fail(err);
            else 
                success(data);
        });
    },

    deleteSubject: function(loggedInUserID, subjectName, success, fail) {
        var params = {
            TableName: _gameTimerTable,
            Key: {
                "userid": loggedInUserID,
                "name": subjectName
            }
        };
        _docClient.delete(params, function(err, data) {
            if(err) 
                fail(err);
            else 
                success(data);
        });
    }
}
